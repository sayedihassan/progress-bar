import { renderHook, act } from '@testing-library/react';
import { Breakpoints } from '@utils/constants/breakpoints';
import useBreakpoints from './index';

function windoResize(width:number) {
  window.innerWidth = width;
  window.dispatchEvent(new Event('resize'));
}

describe('useBreakpoints', () => {
  it('returns correct layout when window size is mobile ', () => {
    const { result } = renderHook(() => useBreakpoints());

    act(() => {   
      windoResize(500);
    });
    
    expect(result.current).toEqual(Breakpoints.Mobile);
  });

  it('returns correct layout when window size is tablet ', () => {
    const { result } = renderHook(() => useBreakpoints());

    act(() => {   
      windoResize(800);
    });
    
    expect(result.current).toEqual(Breakpoints.Tablet);
  });

  it('returns correct layout when window size is laptop ', () => {
    const { result } = renderHook(() => useBreakpoints());

    act(() => {   
      windoResize(1100);
    });
    
    expect(result.current).toEqual(Breakpoints.Laptop);
  });

  it('returns correct layout when window size is desktop ', () => {
    const { result } = renderHook(() => useBreakpoints());

    act(() => {   
      windoResize(1480);
    });
    
    expect(result.current).toEqual(Breakpoints.Desktop);
  });
});