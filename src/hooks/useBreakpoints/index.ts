import { useLayoutEffect , useState } from 'react';
import { Breakpoints } from '@utils/constants/breakpoints';

const useBreakpoints = ():Breakpoints => {
  const [breakpoint, setBreakpoint] = useState(Breakpoints.Default);
  
  useLayoutEffect (() => {
    function handleResize() {
      const width = window.innerWidth;
      if(width<=768) {
        setBreakpoint(Breakpoints.Mobile);
      }else if (width<=992){
        setBreakpoint(Breakpoints.Tablet);
      }else if (width<=1200){
        setBreakpoint(Breakpoints.Laptop);
      }else if(width>1200){
        setBreakpoint(Breakpoints.Desktop);
      }
    }

    window.addEventListener('resize', handleResize);
    
    handleResize();

    return () => window.removeEventListener('resize', handleResize);
  }, []);

  return breakpoint;
};

export default useBreakpoints;
