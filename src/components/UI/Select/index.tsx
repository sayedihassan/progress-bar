import React from 'react';
import classNames from 'classnames/bind';
import styles from './styles.module.scss';

const cx = classNames.bind(styles);

type SelectOptions = {
  label: string;
  value: string|number;
}

interface defaultProps {
  name: string;
  options: SelectOptions[];
  defaultValue?:  string|number;
  onChange?: (e: React.ChangeEvent<HTMLSelectElement>) => void;
}

export default function Select({
  name,
  options = [],
  defaultValue = '',
  onChange,
}: defaultProps) {
  
  return (
    <select className={cx('select-input')} name={name} onChange={onChange} defaultValue={defaultValue}>
      {options.map((item,key)=>(<option key={key} value={item.value}>{item.label}</option>))}
    </select>
  );
}
