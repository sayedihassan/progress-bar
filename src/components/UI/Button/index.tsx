import React from 'react';
import classNames from 'classnames/bind';
import styles from './styles.module.scss';

const cx = classNames.bind(styles);

interface defaultProps {
  label: string;
  type?: 'submit' | 'button';
  onClick?: () => void;
}

export default function Button({ label = '', type = 'button', onClick }: defaultProps) {
  return (
    <button
      type={type}
      className={cx('button')}
      onClick={onClick}
    >
      {label}
    </button>
  );
}
