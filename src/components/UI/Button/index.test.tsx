import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Button from './index';

type defaultProps = {
  [key:string]:unknown;
}


function renderScreen(props:defaultProps) {
  render(<Button label='click me' {...props} />);
}

describe('ProgressBar', () => {
  const testfn = jest.fn();
  const props = {
    label: 'click me',
    onClick: testfn
  };

  it('renders button', () => {
    renderScreen(props);

    const button = screen.getByRole('button', {name: 'click me'});

    expect(button).toBeInTheDocument();
  });

  it('renders button with label correctly', () => {
    renderScreen(props);

    const button = screen.getByRole('button', {name: 'click me'});

    expect(button).toHaveTextContent('click me');
  });

  it('should call passed function when button is clicked', async() => {
    renderScreen(props);

    const button = screen.getByRole('button', {name: 'click me'});

    userEvent.click(button);

    await waitFor(() => {
      expect(testfn).toHaveBeenCalled();
    });
  });
});