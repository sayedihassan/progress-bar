import React from 'react';
import classNames from 'classnames/bind';
import styles from './styles.module.scss';

const cx = classNames.bind(styles);

interface defaultProps {
  percentage: number;
}

export default function ProgressBar({ percentage }: defaultProps) {
  const percentageWidth = percentage > 100? 100 : percentage < 0? 0: percentage;
  
  return (
    <div className={cx('progress-bar-wrapper')} data-testid="progress-bar">
      <div className={cx('percentage-status', {
        'percentage-status-maxed': percentage > 100
      })} style={{ width: `${percentageWidth}%` }} data-testid="progress-status"></div>
      <div className={cx('percentage')} data-testid="progress-percentage">{percentage}%</div>
    </div>
  );
}
