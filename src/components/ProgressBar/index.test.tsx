import { render, screen } from '@testing-library/react';
import ProgressBar from './index';

type defaultProps = {
  [key:string]:unknown;
}


function renderScreen(props:defaultProps) {
  render(<ProgressBar percentage={20} {...props} />);
}

describe('ProgressBar', () => {
  const props = {
    percentage: 20
  };
  
  it('renders progress bar', () => {
    renderScreen(props);

    expect(screen.getByTestId('progress-bar')).toBeInTheDocument();
  });
  
  it('renders progress status with correct width', () => {
    renderScreen(props);

    expect(screen.getByTestId('progress-status')).toHaveStyle({'width':'20%'});
  });
  
  it('renders progress status width zero when passed progress is negative', () => {
    renderScreen({percentage: -30});

    expect(screen.getByTestId('progress-status')).toHaveStyle({'width':'0%'});
  });
  
  it('renders progress status width 100% when passed progress is more than 100%', () => {
    renderScreen({percentage: 125});

    expect(screen.getByTestId('progress-status')).toHaveStyle({'width':'100%'});
  });
  
  it('renders progress status correctly when passed progress is more than 100%', () => {
    renderScreen({percentage: 125});

    expect(screen.getByTestId('progress-status')).toHaveClass('percentage-status-maxed');
  });
  
  it('renders progress bar percentage correctly', () => {
    renderScreen(props);

    expect(screen.getByTestId('progress-percentage')).toHaveTextContent('20%');
  });
});