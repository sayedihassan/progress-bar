import { render, screen, waitFor, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Dashboard from './index';


function renderScreen() {
  render(<Dashboard />);
}

describe('ProgressBar', () => {

  it('renders 3 progress bars', () => {
    renderScreen();

    const progressBars = screen.getAllByTestId('progress-bar');
    
    expect(progressBars.length).toEqual(3);
  });

  it('renders progress bar controls', () => {
    renderScreen();
    
    const button1 = screen.getByRole('button', {name: '-25'});
    const button2 = screen.getByRole('button', {name: '-10'});
    const button3 = screen.getByRole('button', {name: '+10'});
    const button4 = screen.getByRole('button', {name: '+25'});

    expect(button1).toBeInTheDocument();
    expect(button2).toBeInTheDocument();
    expect(button3).toBeInTheDocument();
    expect(button4).toBeInTheDocument();
  });

  it('renders progress bar selection with default bar', () => {
    renderScreen();
    
    expect(screen.getByRole<HTMLOptionElement>('option', { name: '#progress 1' }).selected).toBe(true);
  });
  
  it('progress bar changes on default selected bar is increased', async() => {
    renderScreen();
    
    const button3 = screen.getByRole('button', {name: '+10'});

    userEvent.click(button3);

    await waitFor(() => {
      const progressBars = screen.getAllByTestId('progress-bar');
      const progressBar1 = within(progressBars[0]);
      expect(progressBar1.getByText('20%')).toBeInTheDocument();
    });
  });
  
  it('progress bar changes when bar is changed and increased', async() => {
    renderScreen();

    userEvent.selectOptions(
        screen.getByRole<HTMLSelectElement>('combobox'),
        screen.getByRole<HTMLOptionElement>('option', { name: '#progress 2' }),
    );

    await waitFor(() => {
      expect(screen.getByRole<HTMLOptionElement>('option', { name: '#progress 2' }).selected).toBe(true);
    });

    expect(screen.getByRole<HTMLOptionElement>('option', { name: '#progress 1' }).selected).toBe(false);

    const button3 = screen.getByRole('button', {name: '+10'});

    userEvent.click(button3);

    await waitFor(() => {
      const progressBars = screen.getAllByTestId('progress-bar');
      const progressBar2 = within(progressBars[1]);
      expect(progressBar2.getByText('30%')).toBeInTheDocument();
    });
  });
});