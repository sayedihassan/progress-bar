import React, { useState } from 'react';
import classNames from 'classnames/bind';
import styles from './styles.module.scss';
import ProgressBar from '@components/ProgressBar';
import { Button, Select } from '@components/UI';

const cx = classNames.bind(styles);

type ProgressStatsProps ={
  [key:string]: number;
}

export default function Dashboard() {
  const [selectedProgress, setSelectedPrgress] = useState<string>('progress1');
  const [progressStats, setProgressStats] = useState<ProgressStatsProps>({
    'progress1': 10,
    'progress2': 20,
    'progress3': 70
  });
  
  const onSelectProgress = (e: React.ChangeEvent<HTMLSelectElement>)=>{
    setSelectedPrgress(e.target.value);
  };

  const onChangeProgress = (num:number)=>{
    setProgressStats(progress=>{
      const newProgress = progress[selectedProgress] + num;

      return {
        ...progress,
        [selectedProgress]: newProgress >= 0 ? newProgress : 0
      };
    });
  };
  
  return (
    <div className={cx('dashboard-wrapper')}>
      <div className={cx('content')}>
        <h2>Progress Bar Demo</h2>
        <ProgressBar percentage={progressStats.progress1} />
        <ProgressBar percentage={progressStats.progress2} />
        <ProgressBar percentage={progressStats.progress3} />
        <div className={cx('controls')}>
          <Select 
            name='progress-select' 
            options={[
              {label:'#progress 1', value: 'progress1'},
              {label:'#progress 2', value: 'progress2'},
              {label:'#progress 3', value: 'progress3'}
            ]} 
            defaultValue={selectedProgress}
            onChange={onSelectProgress}
          />
          <div className={cx('control-buttons')}>
            <Button onClick={()=>onChangeProgress(-25)} label='-25' />
            <Button onClick={()=>onChangeProgress(-10)} label='-10' />
            <Button onClick={()=>onChangeProgress(10)} label='+10' />
            <Button onClick={()=>onChangeProgress(25)} label='+25' />
          </div>
        </div>
      </div>
    </div>
  );
}