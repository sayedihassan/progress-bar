export enum Breakpoints {
  Mobile = 'mobile',//768
  Tablet = 'tablet',//992
  Laptop = 'laptop',//1200
  Desktop = 'desktop',//1400
  Default = '',
}
